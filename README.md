# Shopify Theme "Happy"

These files belong to a Shopify theme named "Happy" (based off of the 2013 version of purchased theme named "Masonry"). This theme was customized for [Flowers Are Happy](http://flowersarehappy.com/), later used instead by [ATX Flowers](https://atxflowers.com/); it is also used for [Flora Funera](https://florafuneral.com/).

## Customization Beyond "Masonry"

### Features

All custom features were integrated into theme settings.

#### Theme Styles

- Only two theme styles exist _(the "Masonry" theme had four styles)_:
    - Happy Dark
    - Happy Light

#### Checkout Page

- Card Message entry propagates to final order.

- Zip Code entry propagates to final order.
- Zip Code entry adds Delivery "product" that has a price based on Zip Code. *
    - This "product" cannot be manually removed.
    - This "product" is replaced if Zip Code is changed.
    - Edge cases were found; solved as they arose.
    - Defects were found; solved by simplifying logic.
- Zip Code entry method is a configuration choice:
    - select dropdown; searches as user types *
    - clickable image map of Austin, TX *
- Vacation Message display.

> \* Disabling Javascript disables this functionality. Client accepts this drawback.

#### General Pages

- Add UX for "Add to Cart" button (plugin)
- Vacation Message display.

#### Sidebar

- Allow custom sub-text for logo.
    - Position may be above or below.
    - Text font is configurable.

### Code

- See [./assets/yi3.*](./assets/).
    - This is where the inline Javascript is organized into templates.
- Search entire repo for "Yi3: ".
    - This job was always rush work, so I editted existing files inline, but heavily documented all changes.

## Reference

* [Shopify](https://shopify.com/)
* [Shopify: Theme Kit](https://shopify.github.io/themekit/)
    - Used for syncing to and from a Shopfy store.

* [Shopify: Themes: Masonry](https://themes.shopify.com/themes/masonry/)
    - Current version of the original basis for this theme (from 2013); the two are no longer compatible.
